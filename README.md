# ThisCMS- 指针内容社区系统

#### 项目介绍
ThisCMS-指针内容管理系统

[【点击这里下载最新版本】](https://github.com/thisval/thiscms/releases)

官方网站：www.thisvar.com

#### 系统特性
1. 轻量级的内容管理系统。
2. 支持多用户和评论。
3. 采用伪静态URL美化。
4. 零依赖部署安装，无需安装任何Web环境。
5. 数据库支持MySql/MsSQL/PostgreSQL/SQLite。
6. 采用Google开发的语言Golang开发。

#### 系统支持
- Windows操作系统。
- Linux操作系统。

#### 在线文档
- Windows操作系统。
- Linux操作系统。

#### 安装教程
1. 下载版本：
https://gitee.com/thisvar/thiscms/releases

2. 结构目录:
```
|-- thiscms #执行程序（注意：windows包含exe后缀）
|-- README.md #说明文件
|-- CHANGELOG.md #版本记录
|-- LICENCE.md #协议
|-- config #配置目录。
    |--- app.toml #配置文件，安装时自动生成  
|-- public #公共静态资源（可做CDN解析）
    |--- css #CSS样式     
    |--- img #图片资源
    |--- js #JS资源
    |--- layui #LayUI
    |--- tinymce #Tinymce编辑器
    |--- favicon.icon #Icon图标
|-- uploads #上传目录，可配置。
|-- views #模板
    |--- lark #前台默认主题模板      
    |--- admin #后台模板
    |--- sys #系统模板
        |--- email #邮件模板
        |--- install #安装模板
```

3. 启动网站:
下载后解压，执行thiscms程序。

- windows：

方式一： Windows直接双击运行即可。

方式二：按住Shift键，同时点击鼠标右键，点击“在此处打开CMD/PowerShell命令窗口”：

```cmd

  ./thiscms

```
  


- Linux：

使用终端解压：

```shell

  tar -xzf ThisCMS_v1.x.x_xxx.tar.gz

```
执行命令：
```shell

  cd ThisCMS_v1.x.x_xxx
  
  ./thiscms

```


4. 使用浏览器打开以下网址：

http://127.0.0.1:5060/

您将会看到安装向导：
- 第一步是注册协议，点击同意。
- 第二步是填写数据库信息和管理员账号，点击“确定安装”
- 第三步是安装成功，点击确定退出安装程序。


5. 安装成功：

前台地址：http://127.0.0.1:5060/

管理后台： http://127.0.0.1:5060/admin/login

如果能够正常浏览，恭喜您，网站部署成功喇。


#### 常见问题：

- 如果修改配置文件，则需要重启服务。

-  Linux正式环境，建议使用Nginx转发到5060即可。

-  默认端口是5060，如果您想使用80端口，修改config/app.toml里面的port=80即可。

-  如果是Windows环境，建议部署成安装服务。

-  如果是Linux环境，您可以配合supervisord进行管理。



#### 商业授权

- 免费版：仅限于个人非商业用途，基础功能，不可去除底部版权信息。

- 商业版：适合1个网站授权，可去除底部版权信息，享受持续更新、升级变更内容通知。

- 定制版：官方定制服务， 个性需求定制开发，正规、专业。


#### 联系方式

购买授权，技术支持，商业合作，联系方式如下：

微信：ThisVar

邮箱：ThisVar@qq.com

网站：http://www.thisvar.com


#### 版权所有

©2018 指针工作室（ThisVar.com） 保留所有权利。
